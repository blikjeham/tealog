// Keymaps for different views

package main

import (
	"github.com/charmbracelet/bubbles/key"
)

type keyMap struct {
	Edit key.Binding
	Quit key.Binding
}

func (k keyMap) ShortHelp() []key.Binding {
	return []key.Binding{k.Edit, k.Quit}
}

func (k keyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{k.Edit, k.Quit},
	}
}

var keys = keyMap{
	Edit: key.NewBinding(
		key.WithKeys("ctrl+e"),
		key.WithHelp("^e", "Edit"),
	),
	Quit: key.NewBinding(
		key.WithKeys("ctrl+c"),
		key.WithHelp("^c", "Quit"),
	),
}
