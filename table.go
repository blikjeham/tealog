// QSO log table

package main

import (
	// "log"

	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
)

type Table struct {
	table table.Model
}

func NewTable() Table {

	// TODO: show more colums
	columns := []table.Column{
		{Title: "Callsign", Width: 10},
		{Title: "Band", Width: 5},
		{Title: "Mode", Width: 4},
	}

	rows := []table.Row{
		{"PA5BUK", "20m", "SSB"},
		{"PD3BRT", "2m", "FM"},
	}

	t := table.New(
		table.WithColumns(columns),
		table.WithRows(rows),
		table.WithFocused(true),
		table.WithHeight(10),
		table.WithWidth(25),
	)
	s := table.DefaultStyles()
	s.Header = s.Header.
		BorderStyle(border).
		BorderForeground(borderColor).
		BorderBottom(true).
		Bold(false)
	s.Selected = s.Selected.
		Foreground(hlForeground).
		Background(hlBackground).
		Bold(false)

	t.SetStyles(s)
	return Table{t}
}

func (t Table) Init() tea.Cmd {
	return nil
}

func (t Table) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	t.table, cmd = t.table.Update(msg)
	return t, cmd
}

func (t Table) View() string {
	return t.table.View()
}

// Add a new QSO to the log
func (t *Table) Add(call string) {
	rows := append(t.table.Rows(), table.Row{call, "10m", "SSB"})
	t.table.SetRows(rows)
}
