# TeaLog

A QSO logging program written in GO using [BubbleTea](https://charm.sh)

## Installation

Clone this repo, and run `go build src`