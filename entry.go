// The main call sign entry form

package main

import (
	"log"

	// "github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
)

type Form struct {
	input textinput.Model
	keys  formKeyMap
	view  *view
}

func NewForm(v *view) Form {
	input := textinput.New()
	input.Placeholder = "call"
	input.Focus()
	input.Width = 22
	return Form{
		input: input,
		keys:  formKeys,
		view:  v,
	}
}

func (f Form) Init() tea.Cmd {
	return textinput.Blink
}

func (f Form) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, f.keys.New):
			value := f.input.Value()
			log.Printf("adding new entry: %s", value)
			table := f.view.panes[tablePane].(Table)
			table.Add(value)
			f.view.panes[tablePane] = table
			f.input.Reset()
		}
	}
	f.input, cmd = f.input.Update(msg)
	return f, cmd
}

func (f Form) View() string {
	return f.input.View()
}

/* form KEYMAP */

type formKeyMap struct {
	New key.Binding
	keyMap
}

func (k formKeyMap) ShortHelp() []key.Binding {
	return []key.Binding{k.Edit, k.Quit}
}

func (k formKeyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{k.Edit, k.Quit},
	}
}

var formKeys = formKeyMap{
	key.NewBinding(
		key.WithKeys("enter"),
		key.WithHelp("Enter", "New"),
	),
	keys,
}
