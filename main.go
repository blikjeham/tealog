package main

import (
	"fmt"
	"os"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	// "github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type pane int

const (
	formPane pane = iota
	tablePane
	numPanes
)

type view struct {

	// Panes of the window
	panes []tea.Model

	// Are we editing the table
	edit bool

	// Currently focused pane
	focused pane

	// Keybindings
	keys keyMap
	help help.Model
}

func NewView() *view {
	v := &view{}
	v.panes = make([]tea.Model, numPanes)
	v.panes[formPane] = NewForm(v)
	v.panes[tablePane] = NewTable()

	// help and keys
	v.keys = keys
	v.help = help.New()
	return v
}

func (v view) Init() tea.Cmd {
	return nil
}

func (v view) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, v.keys.Quit):
			return v, tea.Quit

		case key.Matches(msg, v.keys.Edit):
			// Focus the next pane
			v.focused++

			// Wrap to first pane if at the end
			if v.focused == numPanes {
				v.focused = formPane
			}
		}
	}
	current, cmd := v.panes[v.focused].Update(msg)
	v.panes[v.focused] = current
	return v, cmd
}

func (v view) View() string {
	var parsed []string
	for i := formPane; i < numPanes; i++ {
		if i == v.focused {
			parsed = append(parsed, activeStyle.Render((v.panes[i].View())))
		} else {
			parsed = append(parsed, inactiveStyle.Render((v.panes[i].View())))
		}
	}
	parsed = append(parsed, v.help.View(v.keys))
	return lipgloss.JoinVertical(lipgloss.Top, parsed...)
}

func main() {
	// Setup debug logging
	f, err := tea.LogToFile("debug.log", "debug")
	if err != nil {
		fmt.Printf("failed to setup logging: %v", err)
		os.Exit(1)
	}
	defer f.Close()

	// Create new view
	m := NewView()

	// Start the program
	if _, err := tea.NewProgram(m, tea.WithAltScreen()).Run(); err != nil {
		fmt.Println("Error running program", err)
		os.Exit(1)
	}
}
