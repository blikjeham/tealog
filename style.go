// Styles for the TUI

package main

import (
	"github.com/charmbracelet/lipgloss"
)

var (
	border       = lipgloss.NormalBorder()
	noBorder     = lipgloss.HiddenBorder()
	borderColor  = lipgloss.Color("240")
	hlForeground = lipgloss.Color("229")
	hlBackground = lipgloss.Color("57")
)

var baseStyle = lipgloss.NewStyle().
	BorderStyle(border).
	BorderForeground(borderColor)

var activeStyle = lipgloss.NewStyle().
	BorderStyle(border).
	BorderForeground(borderColor)

var inactiveStyle = lipgloss.NewStyle().
	BorderStyle(noBorder)
